print(__file__)

numbers = [1, 2, 3, 4, 5, 6]

# expression, iteration over a sequence
# squares = [number ** 2 for number in numbers]

squares = [number ** 2 for number in range(1, 7)]

# set comprehension apply same rules
# squares = {number ** 2 for number in numbers}

print(squares)
def fibonacci():  # generator that generates fibonacci numbers
    # that's infinite generator
    current, previous = 0, 1
    while True:
        current, previous = current + previous, current
        print("Current = {}, previous = {}".format(current, previous))
        yield current


fib = fibonacci()

print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
# swap two values in python
a = 2
b = 3
print("a = {}, b = {}".format(a, b))

a, b = b, a
# in other languages
# temp = a
# a = b
# b = temp
print("a = {}, b = {}".format(a, b))

# Create a generator to return an infinite sequence of odd numbers, starting at 1.
# Print the first 100 numbers, to check that the generator's working correctly


def generate_odd():
    n = 1
    while True:
        yield n
        n += 2


odds = generate_odd()

for i in range(100):
    print(next(odds))

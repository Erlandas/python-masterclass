def factorial(n):
    # factorial n! can also be defined as n * (n-1)!
    """ Calculates n! recursively """
    if n <= 1:
        return 1
    else:
       # print(n / 0)
        return n * factorial(n-1)


try:
    print(factorial(900))
except (RecursionError, OverflowError):
    print("This program can't calculate factorials that large: ", end='')
    print(RecursionError.__doc__)
except ZeroDivisionError:
    print("Division by zero not allowed: {}".format(ZeroDivisionError.__doc__))

print("Program terminating")

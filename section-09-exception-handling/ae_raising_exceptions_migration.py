import ad_raising_exceptions_ducks

flock = ad_raising_exceptions_ducks.Flock()
donald = ad_raising_exceptions_ducks.Duck()
daisy = ad_raising_exceptions_ducks.Duck()
duck3 = ad_raising_exceptions_ducks.Duck()
duck4 = ad_raising_exceptions_ducks.Duck()
duck5 = ad_raising_exceptions_ducks.Duck()
duck6 = ad_raising_exceptions_ducks.Duck()
duck7 = ad_raising_exceptions_ducks.Duck()
percy = ad_raising_exceptions_ducks.Penguin()

flock.add_duck(donald)
flock.add_duck(daisy)
flock.add_duck(duck3)
flock.add_duck(duck4)
flock.add_duck(duck5)
flock.add_duck(duck6)
flock.add_duck(duck7)
flock.add_duck(percy)

flock.migrate()
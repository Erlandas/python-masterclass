import sys


def divide(a, b):
    print("{} / {} = {}". format(a, b, a / b))


def getint(prompt=''):
    while True:
        try:
            number = int(input(prompt))
            return number
        except ValueError:
            print("Values not provided: {}".format(ValueError.__doc__))
        except EOFError:
            print(EOFError.__doc__)
            sys.exit(0)


number_one = getint("Please enter a first number: ")
number_two = getint("Please enter a second number: ")

try:
    divide(number_one, number_two)
except ZeroDivisionError:
    print("Division by zero: {}".format(ZeroDivisionError.__doc__))


# # opening file using function open(path, permission)
# # jabber = open(r"/home/erlandas/Git/python/python-masterclass/section-05-input-and-output-(IO)/sample.txt", 'r')
# # if file in packages folder we can directly point to file
# # relative path
# jabber = open("sample.txt", 'r')
# # read each line and print to console
# for line in jabber:
#     if "jabberwock" in line.lower():
#         print(line, end='')
#
# # close stream
# jabber.close()

# opening file using with
# using with we dont need to close file as with takes care of closing files
# with statement deals with exceptions
# if there is an error in a file it will trap the error and close the file
# with open("sample.txt", 'r') as jabber:
#     for line in jabber:
#         if "JAB" in line.upper():
#             print(line, end='')

# USING READLINE()
# process file line by line
# with open("sample.txt", 'r') as jabber:
#     line = jabber.readline()
#     while line:
#         print(line, end='')
#         line = jabber.readline()

# USING READLINES()
# reads entire file
# that's will read file and return list of strings as each line
# with open("sample.txt", 'r') as jabber:
#     lines = jabber.readlines()
# print(lines)
#
# for line in lines[::-1]:
#     print(line, end='')

# USING READ()
# reads entire file and returns string contents of a file
with open("sample.txt", 'r') as jabber:
    lines = jabber.read()

for line in lines[::-1]:
    print(line, end='')
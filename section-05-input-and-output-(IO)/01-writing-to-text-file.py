# cities = ["Adelaide", "Alice Springs", "Darwin", "Melbourne", "Sydney"]

# same as writing just open(file, permission)
# if file does not exists it will be created on demand
# if file exists it will be overwritten
# flush parameter stores contents straight from the buffer to file
# otherwise all file will be stored in a buffer and then written
# with open("cities.txt", 'w') as city_file:
#     for city in cities:
#         print(city, file=city_file, flush=True)

# Reading file we just created
# Strip() function removes what is specified
# strip removes only from the beginning or the end of a string
# cities = []
#
# with open("cities.txt", 'r') as city_file:
#     for city in city_file:
#         cities.append(city.strip('\n'))
#
# print(cities)
# for city in cities:
#     print(city)

# imelda = "More Mayhem", "Imelda May", "2011",(
#     (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
# )
#
# with open("imelda3.txt", 'w') as imelda_file:
#     print(imelda, file=imelda_file)

with open("imelda3.txt", 'r') as imelda_file:
    contents = imelda_file.readline()
imelda = eval(contents)

print(imelda)
title, artists, year, tracks = imelda
print(title)
print(artists)
print(year)
print(tracks)

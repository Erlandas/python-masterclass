import shelve
# NOTE: shelve key must be a string
# with shelve.open("shelf_test") as fruit:
#     fruit['orange'] = "a sweet, orange, citrus fruit"
#     fruit['apple'] = "good for making cider"
#     fruit['lemon'] = "a sour, yellow citrus fruit"
#     fruit['grape'] = "a small, sweet fruit growing in bunches"
#     fruit['lime'] = "a sour, green citrus fruit"
#
#     print(fruit["lemon"])
#     print(fruit["grape"])
#
# print(fruit)

fruit = shelve.open("shelf_test")
# fruit['orange'] = "a sweet, orange, citrus fruit"
# fruit['apple'] = "good for making cider"
# fruit['lemon'] = "a sour, yellow citrus fruit"
# fruit['grape'] = "a small, sweet fruit growing in bunches"
# fruit['lime'] = "a sour, green citrus fruit"

# print(fruit["lemon"])
# print(fruit["grape"])
#
# fruit["lime"] = "great with tequila"
#
# for snack in fruit:
#     print("{}: {}".format(snack, fruit[snack]))

# while True:
#     dict_key = input("Please enter a fruit: ")
#     if dict_key == "quit":
#         break
#
#     if dict_key in fruit:
#         desc = fruit[dict_key]
#         print(desc)
#     else:
#         print("We don't have a {}".format(dict_key))

    # description = fruit.get(dict_key, "We don't have a {}".format(dict_key))
    # print(description)

# ordered_keys = sorted(list(fruit.keys()))
# for f in ordered_keys:
#     print("{} - {}".format(f, fruit[f]))

for v in fruit.values():
    print(v)

print(fruit.values())

print("=" * 50)

for f in fruit.items():
    print(f)

print(fruit.items())

fruit.close()

print(fruit)
import  shelve

# with shelve.open("bike") as bike:
#     bike["make"] = "Honda"
#     bike["model"] = "250 dream"
#     bike["colour"] = "red"
#     bike["engine_size"] = 250
#
#     print(bike["engine_size"])
#     print(bike["colour"])

# if we do typo and we fix it it will stay as a key in a shelve file
with shelve.open("bike2") as bike:
    # bike["make"] = "Honda"
    # bike["model"] = "250 dream"
    # bike["colour"] = "red"
    # bike["engin_size"] = 250

    # to remove entry
    del bike["engin_size"]

    for key in bike:
        print(key)

    print("=" * 40)

    print(bike["engine_size"])
    print(bike["colour"])
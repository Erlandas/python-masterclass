#          01234567890123456789012345
letters = "abcdefghijklmnopqrstuvwxyz"

backwards = letters[::-1]   #reversing the sequence
print(backwards)

print(letters[16:13:-1])    #qpo
print(letters[4::-1])       #edcba
print(letters[:17:-1])      #last 8 characters in reverse order
#OR
print(letters[:-9:-1])

#get last 4 letters
print(letters[-4:])
print(letters[-1:]) #last letter
print(letters[:1])  #first letter
print(letters[0])   #same result for first letter just using index position

#using {0:2} in a replecament field specifies how many places it is given for the value aka width
for i in range(1, 13):
    print("No. {0:2} squared is {1:3} and cubed is {2:4}"
          .format(i, i ** 2, i ** 3))

print()

#to make values left allign use <
#to make values right allign use >
#to make values centered use ^
for i in range(1, 13):
    print("No. {0:>2} squared is {1:<3} and cubed is {2:^4}"
          .format(i, i ** 2, i ** 3))

print()

print("Pi is aproximately {0:12}".format(22 / 7))
print("Pi is aproximately {0:12f}".format(22 / 7))
print("Pi is aproximately {0:12.50f}".format(22 / 7))
print("Pi is aproximately {0:52.50f}".format(22 / 7))
print("Pi is aproximately {0:62.50f}".format(22 / 7))
print("Pi is aproximately {0:<72.50f}".format(22 / 7))
print("Pi is aproximately {0:<72.54f}".format(22 / 7))

print()

for i in range(1, 13):
    print("No. {} squared is {} and cubed is {:4}"
          .format(i, i ** 2, i ** 3))

print()
#another way to format strings using f before string
print(f"Pi is aproximately {22 / 7:12.50f}")
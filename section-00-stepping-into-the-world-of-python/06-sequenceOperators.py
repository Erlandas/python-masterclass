string1 = "he's "
string2 = "probably "
string3 = "pining "
string4 = "for the "
string5 = "fjords"

print(string1 + string2 + string3 + string4 + string5)

print("Hello " * 5)

print("Hello " * (5 + 4))
print("Hello " * 5 + "4")

#to check if a given string is a substring of a given string
# in operator evaluates to true if first exists in a second one
today = "friday"
print("day" in today)      #true
print("fri" in today)       #true
print("thur" in today)      #false
print("parrot" in "fjord")  #false
#Indexing 01234567890123
#negative 43210987654321
parrot = "Norwegian Blue"

print(parrot)

print(parrot[3])
print(parrot[4])
print(parrot[9])
print(parrot[3])
print(parrot[6])
print(parrot[8])

#Negative indexing we starting from -1
print()

print(parrot[-11])
print(parrot[-10])
print(parrot[-5])
print(parrot[-11])
print(parrot[-8])
print(parrot[-6])

print()

print(parrot[3-14])
print(parrot[4-14])
print(parrot[9-14])
print(parrot[3-14])
print(parrot[6-14])
print(parrot[8-14])

#String slicing
print()
print(parrot[0:6]) # Norweg
print(parrot[3:5]) # we
print(parrot[0:9]) # Norwegian
print(parrot[:9])
print(parrot[10:])
print(parrot[:6])
print(parrot[6:])

print(parrot[:6] + parrot[6:])
print(parrot[:])
print("--------------")

#Negative slicng
print(parrot[-4:-2])    # bl
print(parrot[-4:12])    # bl

#Using step in a slice
print()
#[start:end:step]
print(parrot[0:6:2])    #Nre
print(parrot[0:6:3])    #Nw

number = "9,123;123:123 123,123;123"
seperators = number[1::4]
print(seperators)

values = "".join(char if char not in seperators else " " for char in number).split()
print([int(val) for val in values])


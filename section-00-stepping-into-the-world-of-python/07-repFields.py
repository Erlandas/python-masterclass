age = 24;
#str() function makes variable a string
print("My age is " + str(age) + " years")

#using string format fuction
print("My age is {0} years".format(age))

print("There are {0} days in {1}, {2}, {3}, {4}, {5}, {6} and {7}"
      .format(31, "Jan", "Mar", "May", "Jul", "Aug", "Oct", "Dec"))

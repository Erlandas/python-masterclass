for i in range(17):
    print("{0:>2} in hex is {0:>02x}".format(i))

# to represent hex number we use 0x in front of a number
x = 0x20
y = 0x0a
# this way will print decimal equivalent
# to print hex number use
# {0:02x} passing value .format(x * y)
print(x)
print(y)
print(x * y)

a = 0xf
b = 0x1
c = a + b
print("{0:>02x}hex".format(c))
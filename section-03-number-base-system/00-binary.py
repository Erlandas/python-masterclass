# to print in binary given integer value
for i in range(17):
    print("{0:>2} in binary is {0:>08b}".format(i))

# to represent binary numbers we use 0b in front
binary = 0b01010100
print(binary)   # prints binary equivalent in decimal

# TO NOTE:
# attribute that start with single underscore "_"
# are for internal use only
# "__" before variable name prevents variable to be changed
# still can be changed using _Class_attr = value
# preventing accidental shadowing

import datetime
import pytz


class Account:
    """ Simple account class with balance """

    # Static method
    # Static methods are shared between all instances of a class
    @staticmethod
    def _current_time():
        utc_time = datetime.datetime.utcnow()
        return pytz.utc.localize(utc_time)

    def __init__(self, name, balance):
        self._name = name
        self.__balance = balance
        self._transaction_list = [(Account._current_time(), balance)]
        print("Account created for {}".format(self._name))

    def deposit(self, amount):
        if amount > 0:
            self.__balance += amount
            self.show_balance()
            self._transaction_list.append((Account._current_time(), amount))

    def withdraw(self, amount):
        if 0 < amount <= self.__balance:
            self.__balance -= amount
            self._transaction_list.append((Account._current_time(), -amount))
        else:
            print("Amount must be greater than zero and no more then your own balance")
        self.show_balance()

    def show_balance(self):
        print("Balance is {}".format(self.__balance))

    def show_transactions(self):
        for date, amount in self._transaction_list:
            if amount > 0:
                tran_type = "deposited"
            else:
                tran_type = "withdrawn"
                amount *= -1
            print("{:6} {} on {} (local time was {})"
                  .format(amount, tran_type, date, date.astimezone()))


if __name__ == '__main__':
    erlandas = Account("Erlandas", 0)
    erlandas.show_balance()
    erlandas.deposit(1000)
    erlandas.withdraw(500)
    erlandas.withdraw(5000)

    erlandas.show_transactions()

    steph = Account("Steph", 800)
    steph.__balance = 20000  # does not change obj attribute
    steph.deposit(100)
    steph.withdraw(200)
    steph.show_transactions()

class Wing(object):

    def __init__(self, ratio):
        self.ratio = ratio

    def fly(self):
        if self.ratio > 1:
            print("Wee this is fun")
        elif self.ratio == 1:
            print("It is hard but i am flying")
        else:
            print("I thin i will walk")


class Duck(object):

    def __init__(self):
        self._wing = Wing(1.8)

    def walk(self):
        print("Duck walks")

    def swim(self):
        print("Duck swims")

    def quack(self):
        print("Quack quack")

    def fly(self):
        self._wing.fly()


class Penguin(object):

    def walk(self):
        print("Penguin walks")

    def swim(self):
        print("Penguin swims")

    def quack(self):
        print("Penguin quack")


if __name__ == "__main__":
    donald = Duck()
    donald.fly()

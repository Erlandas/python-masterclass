from player import Player
from enemy import Enemy, Troll, Vampyre, VampyreKing


tim = Player("Tim")

ugly_troll = Troll("Pug")
print("Ugly troll - {}".format(ugly_troll))

another_troll = Troll("Ug")
print("Another troll - {}".format(another_troll))

troll = Troll("Urg")
print(troll)

ugly_troll.grunt()
another_troll.grunt()
troll.grunt()
ugly_troll.take_damage(12)

print("=" * 80)

vamp = Vampyre("Vamp")
vlad = Vampyre("Vlad")
print(vamp)
print(vlad)
# vamp.take_damage(30)
print(vamp)

print("-" * 80)
print(8 / 2)
print(8 // 3)
print(8 - 2.5 - 1.5)
print("-" * 80)
vamp_king = VampyreKing("King")
while vamp_king._alive == True:
    vamp_king.take_damage(10)

# while vamp._alive:
#     vamp.take_damage(1)


# random_monster = Enemy("Basic enemy", 12, 1)
# print(random_monster)
#
# random_monster.take_damage(4)
# print(random_monster)
#
# random_monster.take_damage(14)
# print(random_monster)


# print(tim.name)
# print(tim.lives)
# tim.lives -= 1
# print(tim)
#
# tim.lives -= 1
# print(tim)
#
# tim.lives -= 1
# print(tim)
#
# tim.lives -= 1
# print(tim)
#
# tim.level = 2
# print(tim)
#
# tim.level += 5
# print(tim)
#
# tim.level = -1
# print(tim)
#
# tim.score = 500
# print(tim)

class Duck(object):

    def walk(self):
        print("Duck walks")

    def swim(self):
        print("Duck swims")

    def quack(self):
        print("Quack quack")


class Penguin(object):

    def walk(self):
        print("Penguin walks")

    def swim(self):
        print("Penguin swims")

    def quack(self):
        print("Penguin quack")


def test_duck(duck):
    duck.walk()
    duck.swim()
    duck.quack()


if __name__ == "__main__":
    donald = Duck()
    test_duck(donald)

    percy = Penguin()
    test_duck(percy)

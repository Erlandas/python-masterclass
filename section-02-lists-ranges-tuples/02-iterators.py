string = "1234567890"

# for char in string:
#     print(char)

my_iterator = iter(string)
print(my_iterator)
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))
# print(next(my_iterator))

# that's what for loop does creates iterator
# for char in iter(string):
#     print(char)

number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
number_list_iterator = iter(number_list)

for num in range(len(number_list)):
    print(next(number_list_iterator))
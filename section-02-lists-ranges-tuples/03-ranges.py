print(range(100))
my_list = list(range(10))
print(my_list)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))
reverse = list(range(10,-1,-1))

print(even)
print(odd)
print(reverse)

my_string = "abcdefghijklmnopqrstuvwxyz"
print(my_string.index("e"))
print(my_string[4])

small_decimals = range(0, 10)
print(small_decimals)
print(small_decimals.index(3))

odd = range(1, 10000, 2)
print(odd)
print(odd[985])

sevens = range(7, 1000000, 7)
#x = int(input("Please enter a positive number less than one million: "))
x = 0
if x in sevens:
    print("{} is divisible by seven".format(x))

print("-" * 30)

print(small_decimals)
my_range = small_decimals[::2]
print(my_range)
print(my_range.index(4))

print("-" * 30)

decimals = range(0, 100)
print(decimals)

my_range_two = decimals[3:40:3]
print(my_range_two)

for i in my_range_two:
    print(i)

print("-" * 30)

for i in range(3, 40, 3):
    print(i)

print(my_range_two == range(3, 40, 3))
print("-" * 30)
# it will return true as it does testing on actual contents of ranges
print(range(0, 5, 2) == range(0, 6, 2))
print(list(range(0, 5, 2)))
print(list(range(0, 6, 2)))
print("-" * 30)
print("MORE EXAMPLES")
# More examples of range and slicing
r = range(0, 100)
print(r)

for i in r[::-2]:
    print(i)

print("-" * 30)

for i in range(99, 0, -2):
    print(i)

print("-" * 30)
print(range(0, 100)[::-2] == range(99, 0, -2))

# Reverse string example
my_name = "Erlandas Bacauskas"
print(my_name[::-1])
print("@" * 30)
# Mini challenge
o = range(0, 100, 4)
print(o)
p = o[::5]
print(p)
for i in p:
    print(i)















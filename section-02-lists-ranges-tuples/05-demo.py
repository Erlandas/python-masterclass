# same value assigned for each variable
a = b = c = d = 12
print(c)

# different value assigned for each variable
a, b = 12, 13
print(a, b)

# swap variable values
a, b = b, a
print("a is {}".format(a))
print("b is {}".format(b))

# ip_address = input("Please enter an IP address: ")
# print(ip_address.count("."))

parrot_list = ["non pinin", "no more", "a stiff", "bereft of live"]
parrot_list.append("A Norwegian Blue")
for state in parrot_list:
    print("This parrot is {}".format(state))

even_numbers = [2, 4, 6, 8]
odd_numbers = [1, 3, 5, 7, 9]

numbers = even_numbers + odd_numbers
print(numbers)
# method below works on actual list not returning value
# destructive operation modifies list
# numbers.sort()
print(numbers)
# if we want list returned as sorted use method sorted(list)
# by passing list as argument
# to note does not modifies actual list
numbers_in_order = sorted(numbers)
print(numbers_in_order)
print(numbers)

# comparing each element in its place even if it has same elements
# but different order it will return false
if numbers == numbers_in_order:
    print("The lists are equal")
else:
    print("The lists are not equal")

if numbers_in_order == sorted(numbers):
    print("The lists are equal")
else:
    print("The lists are not equal")

print("-" * 30)

# Using list constructor
# To note both declarations below gives same result empty list
list_one = []           # creating empty list
list_two = list()       # calling list constructor

print("List 1: {}".format(list_one))
print("List 2: {}".format(list_two))

if list_two == list_one:
    print("The lists are equal")
else:
    print("The lists are not equal")

even = [2, 4, 6, 8]
# this way points to a memory
# another_even = even
# if we want separate list send list as argument
another_even = list(even)

print(another_even is even)

another_even.sort(reverse=True)
print(even)
print("-" * 30)

# 2D list example
number_lists = [even_numbers, odd_numbers]
# Iterating through 2D list
for number_set in number_lists:
    print(number_set)

    for value in number_set:
        print(value)
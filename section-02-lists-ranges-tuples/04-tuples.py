# Tuples are imutable that means once they created they can not be changed
# t = ("a", "b", "c")   # tuple
# print(t)
# print("a", "b", "c")
# print(("a", "b", "c"))

welcome = "Welcome to my Nightmare", "Alice Cooper", 1975
bad = "Bad Company", "Bad Company", 1974
budgie = "Nightlight", "Budgie", 1981
imelda = "More Mayhem", "Imelda May", 2011, (
    (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz"))
metallica = "Ride the Lightning", "Metallica", 1984

print(metallica)
print(metallica[0])
print(metallica[1])
print(metallica[2])

# Produces an error
# metallica[0] = "Master of Puppets"

# To correctly update tuple values
# imelda = imelda[0], "Imelda May", imelda[2]

# When lists are used we can change values assigned
# Term for this unpacking tuple
metallica2 = ["Ride the Lightning", "Metallica", 1984]
print(metallica2)
metallica2[0] = "Master od Puppets"
print(metallica2)

# extracting values into a variables from a tuple
title, artist, year, tracks = imelda
print(title)
print(artist)
print(year)
print(tracks)   # extracted as tuple of tuples

print("=" * 50)

imelda = "More Mayhem", "Imelda May", 2011, (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
# extracting individual songs
title, artist, year, track1, track2, track3, track4 = imelda
print(title)
print(artist)
print(year)
print(track1)
print(track2)
print(track3)
print(track4)
print("=" * 50)

# challenge part
imelda = "More Mayhem", "Imelda May", 2011, (
    (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz"))
title, artist, year, tracks = imelda
print(title, artist, year)
imelda_iterator = iter(tracks)
for track in range(len(tracks)):
    current_track = next(imelda_iterator)
    print("{}.\t{}".format(current_track[0],current_track[1]))

print("=" * 50)
# example of tuple containing list
imelda = "More Mayhem", "Imelda May", 2011, (
    [(1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")])

# now we can append to a list
imelda[3].append((5, "All For You"))
title, artist, year, tracks = imelda
print(title, artist, year)
for song in tracks:
    track, title = song
    print("\tTrack number {}, Title: {}".format(track, title))

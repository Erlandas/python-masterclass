my_list = ["a", "b", "c", "d"]
new_string = ""

# that's not a good way
# every time it loops it is creating new string
# not as efficient ant slow
for c in my_list:
    new_string += c + ", "

print(new_string)

# better way using join below
join_string = ", ".join(my_list)
print(join_string)

# more examples
letters = "abcdefghijklmnopqrstuvwxyz"
join_letters = ", ".join(letters)
print(join_letters)
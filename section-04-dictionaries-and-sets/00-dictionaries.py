# dictionary key: value pairs
fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit"}

# reaching values by using key
print(fruit)
print(fruit["lemon"])

fruit["pear"] = "an odd shaped apple"

for key in fruit:
    print("{0:<7s}: {1}".format(key, fruit[key]))
print("=" * 70)
# to update values
# to note no duplicate key's allowed if we giving new value for a key
# we replacing old one
fruit["lime"] = "great with tequila"

for key in fruit:
    print("{0:<7s}: {1}".format(key, fruit[key]))
print("=" * 70)
# to remove entry from a collection
# we use del function
del fruit["lemon"]
for key in fruit:
    print("{0:<7s}: {1}".format(key, fruit[key]))

# to delete all dictionary we use .clear() function
# fruit.clear()
# print(fruit)
print("=" * 70)

# to get values we using .get(key) function
# if we will try to access value by using unknown key on its own
# we will get error: fruit["non existing key"] equals to crash
# print(fruit["random key"])
while True:
    dict_key = input("Please enter a fruit: ")
    if dict_key == "quit":
        break
    # if dict_key in fruit:
    #     description = fruit.get(dict_key)
    #     print(description)
    # else:
    #     print("We don't have a {}".format(dict_key))
    # Alternative way second argument is default value
    description = fruit.get(dict_key, "We don't have a {}".format(dict_key))
    print(description)

# in older versions of python every time we access dictionary
# we getting different order of keys and values
# to prevent this add keys to a list, sort list, than iterate through
# in case its needed this way we get always in the same order
# ordered_keys = list(fruit.keys())
# ordered_keys.sort()
# equivalent
ordered_keys = sorted(list(fruit.keys()))
for fruit_key in ordered_keys:
    print("{0:<9} -> {1}".format(fruit_key, fruit[fruit_key]))

# to get values only fruit.values()
# to note it is not as efficient as reaching values using keys
# use keys to reach values preferably
# both commands below gives reference to object
# pointing to memory location where values/keys are stored
print(fruit.keys())
print(fruit.values())

fruit_keys = fruit.keys()
print(fruit_keys)

# we not modifying directly fruit_keys
# value updated as its pointing to dictionary object
fruit["tomato"] = "not nice with ice cream"
print(fruit_keys)

print("=" * 70)
# dictionary is like list of tuples example below
print(fruit.items())
# to create tuple out of dictionary use example below
fruit_tuple = tuple(fruit.items())
print(fruit_tuple)

# reminder how to reach items from tuple
for snack in fruit_tuple:
    item, description = snack
    print("{0:<8} is {1}".format(item, description))

# to create a dictionary from tuple we use
# built in python function dict(tuple)
print("=" * 70)
print(dict(fruit_tuple))

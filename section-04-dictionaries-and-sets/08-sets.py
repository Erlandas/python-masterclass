# farm_animals = {"sheep", "cow", "hen"}
# print(farm_animals)
#
# for animal in farm_animals:
#     print(animal)
#
# print("=" * 40)
#
# wild_animals = set(["lion", "tiger", "panther", "elephant", "hare"])
# print(wild_animals)
#
# for animal in wild_animals:
#     print(animal)
#
# farm_animals.add("horse")
# wild_animals.add("horse")
# print(farm_animals)
# print(wild_animals)
#
# print("=" * 70)
# # to create empty set we have to call set()
# # we cant use empty {} as it will create dictionary
# empty_set = set()
#
# even = set(range(0, 40, 2))
# print(even)
# squares_tuple = (4, 6, 9, 16, 25)
# squares = set(squares_tuple)
# print(squares)

even = set(range(0, 40, 2))
print(even)
print(len(even))

squares_tuple = (4, 6, 9, 16, 25)
squares = set(squares_tuple)
print(squares)
print(len(squares))

print(even.union(squares))
print(len(even.union(squares)))

print("=" * 70)

print(even.intersection(squares))
print(even & squares)
print(squares.intersection(even))
print(squares & even)

# NOTE: .difference function returns set A - set B without modifying sets
# .difference_update modifies set but does not return anything
print("=" * 70)
print("even minus squares")
print(sorted(even.difference(squares)))
print(sorted(even - squares))

print("squares minus even")
print(sorted(squares.difference(even)))
print(sorted(squares - even))

print("=" * 70)
print(sorted(even))
print(squares)
#even.difference_update(squares)
print(sorted(even))

print("=" * 70)
# assimetric difference
print(sorted(even))
print(squares)

print("symmetric even minus squares")
print(sorted(even.symmetric_difference(squares)))
print("symmetric squares minus even")
print(squares.symmetric_difference(even))

# # remove does raise error discard doesnt if item does not exists
# print("=" * 70)
# squares.discard(4)
# squares.remove(16)
# squares.discard(8)  #  no error does nothing
# print(squares)
# # squares.remove(8)   #  we should get error
#
# # exception example
# try:
#     squares.remove(8)
# except KeyError:
#     print("The item '8' is not a member of the set")

print("=" * 70)
if squares.issubset(even):
    print("squares is a subset of even")

if even.issuperset(squares):
    print("even is a superset of squares")

# frozen sets
# frozen set is immutable once created we can not modify it
new_even = frozenset(range(0, 100, 2))
print(new_even)
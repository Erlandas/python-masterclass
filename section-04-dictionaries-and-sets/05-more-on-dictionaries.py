fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit"}

print(fruit)

veg = {"cabbage": "every child's favourite",
       "sprouts": "mmmm, lovely",
       "spinach": "can I have some more fruit, please"}

print(veg)
print("=" * 70)
print("UPDATE FUNCTION")
# Update method allows to combine two dictionaries together
veg.update(fruit)
for snack in veg:
    print("{0:<8} -> {1}".format(snack, veg[snack]))

print("=" * 70)
print("COPY FUNCTION")
# Copy method allows to create a copy of a dictionary
nice_and_nasty = fruit.copy()
nice_and_nasty.update(veg)

for key in nice_and_nasty:
    print("{0:<8} -> {1}".format(key, nice_and_nasty[key]))

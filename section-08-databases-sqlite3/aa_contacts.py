import sqlite3


# this line will create DB if it is not exist otherwise it will open
db = sqlite3.connect("contacts.sqlite")
db.execute("CREATE TABLE IF NOT EXISTS contacts (name TEXT, phone INTEGER, email TEXT)")
db.execute("INSERT INTO contacts (name, phone, email) VALUES('Power Girl', 123456, 'tim@email.com')")
db.execute("INSERT INTO contacts VALUES('Batman', 1234, 'brian@email.com')")

# create cursor to iterate through table
cursor = db.cursor()
cursor.execute("SELECT * FROM contacts")

# fetchall() returns list containing a tuple
#print(cursor.fetchall())

# once we iterate through row nothing is left in a cursor()
# if we want to reset we have to run query again
for name, phone, email in cursor:
    print(name)
    print(phone)
    print(email)
    print("-" * 20)

cursor.close()
db.commit()  # committing changes to DB
db.close()

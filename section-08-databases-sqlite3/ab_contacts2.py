import sqlite3


# this line will create DB if it is not exist otherwise it will open
db = sqlite3.connect("contacts.sqlite")

new_email = "CCCCCC@BBB.com"
phone = input("Please enter a phone number: ")

# update_sql = "UPDATE contacts SET email = '{}' WHERE contacts.phone = {}".format(new_email, phone)
update_sql = "UPDATE contacts SET email = ? WHERE contacts.phone = ?"
print(update_sql)

update_cursor = db.cursor()
# executescript allows to run multiple sql separated by ; prone for SQL injections
# update_cursor.executescript(update_sql)
# using ? similar as prepared statements in JDBC
update_cursor.execute(update_sql, (new_email, phone))
print("{} rows updated".format(update_cursor.rowcount))
update_cursor.connection.commit()
update_cursor.close()

print("=" * 40)
print("Are connections is the same: {}".format(update_cursor.connection == db))
print("=" * 40)


for name, phone, email in db.execute("SELECT * FROM contacts"):
    print(name)
    print(phone)
    print(email)
    print("-" * 20)

db.close()

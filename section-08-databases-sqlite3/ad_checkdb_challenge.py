import sqlite3

conn = sqlite3.connect("contacts.sqlite")

get_name = input("Enter a name: ")
sql_retrieve = "SELECT * FROM contacts WHERE name = ?"

for row in conn.execute(sql_retrieve, (get_name,)):
    print(row)

conn.close()

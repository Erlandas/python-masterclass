menu_options = """Please choose an option:
1.\tOption One
2.\tOption Two
3.\tOption Three
4.\tOption Four
5.\tOption Five
6.\tOption Six
0.\tExit"""

choice = "-"
while choice != "0":
    if choice in "123456":
        print("You chose {}".format(choice))
    else:
        print(menu_options)
    choice = input("Enter option: ")

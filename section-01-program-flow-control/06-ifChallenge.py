name = input("Please enter your name: ")
age = int(input("Hello {}, what age are you? ".format(name)))

if 18 <= age < 31:
    print("{} you are welcome to our holidays".format(name))
elif age < 18:
    print("Sorry {0}, come back in {1} years.".format(name, 18 - age))
else:
    print("Sorry {0}, you are {1} years to late.".format(name, age - 30))
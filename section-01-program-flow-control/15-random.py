import random

highest = 10
answer = random.randint(1, highest)

print("Testing answer: {}".format(answer))
print("Type 0 to terminate... Good luck!")
guess = -1
print("Please guess a number between 1 and {}: ".format(highest))
while guess != answer:
    guess = int(input())

    if guess == answer:
        print("You got it!")
        break
    elif guess == 0:
        print("Bye Bye")
        break
    else:
        if guess < answer:
            print("Please guess higher")
        else:
            print("Please guess lower")





# if guess == answer:
#     print("You got it first time")
# else:
#     if guess < answer:
#         print("Please guess higher")
#     else:
#         print("Please guess lower")
#     guess = int(input())
#     if guess == answer:
#         print("Well done, you guessed it!")
#     else:
#         print("Sorry, you have not guessed correctly")

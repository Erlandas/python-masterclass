import turtle
# import time
# instead of importing as above we can do like this
from turtle import forward, right
forward(100)

turtle.forward(150)
right(250)
turtle.forward(150)
turtle.right(250)
turtle.forward(250)
turtle.circle(75)

# time.sleep(4)
turtle.done()

# def func-name
# all functions in python return value
# if return value not specified returns NONE


def python_food():
    width = 80
    text = "Spam and eggs"
    left_margin = (width - len(text)) // 2
    print(" " * left_margin, text)

# BEFORE CHANGES
# def center_text(*args, sep=' ', end='\n', file=None, flush=False):  # * variable amount of parameters
#     text = ""
#     for arg in args:
#         text += str(arg) + sep
#     # text = str(text)  # if number passed it is converted to a string
#     left_margin = (80 - len(text)) // 2
#     print(" " * left_margin, text, end=end, file=file, flush=flush)
#
#
# with open("centered", mode='w') as centered_file:
#     # call the function
#     # python_food()
#     # print("Return value {}".format(python_food()))
#     center_text("Spam and eggs", file=centered_file)
#     center_text("Spam, spam and eggs", file=centered_file)
#     center_text(12, file=centered_file)
#     center_text("Spam, spam, spam and spam", file=centered_file)
#     center_text("first", "second", 3, 4, "spam", file=centered_file)


def center_text(*args, sep=' '):  # * variable amount of parameters
    text = ""
    for arg in args:
        text += str(arg) + sep
    # text = str(text)  # if number passed it is converted to a string
    left_margin = (80 - len(text)) // 2
    return " " * left_margin + text


print(center_text("Spam and eggs"))
print(center_text("Spam, spam and eggs"))
print(center_text(12))
print(center_text("Spam, spam, spam and spam"))
print(center_text("first", "second", 3, 4, "spam", sep=":"))

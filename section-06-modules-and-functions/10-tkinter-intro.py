import tkinter

# try:
#     import tkinter
# except ImportError:  # python 2
#     import Tkinter as tkinter

print(tkinter.TkVersion)
print(tkinter.TclVersion)

# tkinter._test()
main_window = tkinter.Tk()

main_window.title("Hello World")
# (resolution+pixels from left+pixels from top)
main_window.geometry("640x480+100+300")

# .mainLoop() passes control to tk this way window works
main_window.mainloop()

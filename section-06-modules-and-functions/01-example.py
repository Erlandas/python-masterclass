# simply prints what is already imported
# print(dir())
#
# print(dir(__builtins__))
#
# for m in dir(__builtins__):
#     print(m)

import shelve
# print(dir())
# print(dir(shelve))
#
# for obj in dir(shelve.Shelf):
#     if obj[0] != '_':
#         print(obj)

# gives documentation of module similar to man command in linux
# help(shelve)

import random
help(random.randint)

import tkinter

main_window = tkinter.Tk()

main_window.title("Hello World")
# (resolution+pixels from left+pixels from top)
main_window.geometry("840x680+100+300")
# .mainLoop() passes control to tk this way window works

# we create component (specify window, add text)
label = tkinter.Label(main_window, text="Hello World")
# add using geometry manager specify position 'top'
label.pack(side='top')

# .frame(for what parent frame it belongs)
left_frame = tkinter.Frame(main_window)
left_frame.pack(side='left', anchor='n', fill=tkinter.Y, expand=False)

canvas = tkinter.Canvas(left_frame, relief='raised', borderwidth=1)
canvas.pack(side='left', anchor='n')
# canvas.pack(side='top', fill=tkinter.BOTH, expand=True)

right_frame = tkinter.Frame(main_window)
right_frame.pack(side='right', anchor='n', expand=True)

# Button(for what frame belongs, text)
button_1 = tkinter.Button(right_frame, text="button1")
button_2 = tkinter.Button(right_frame, text="button2")
button_3 = tkinter.Button(right_frame, text="button3")
# anchor takes parameter n = north, s = south ...
button_1.pack(side='top')
button_2.pack(side='top')
button_3.pack(side='top')

main_window.mainloop()

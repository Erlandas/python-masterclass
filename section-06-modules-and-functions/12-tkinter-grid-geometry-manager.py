import tkinter

main_window = tkinter.Tk()

main_window.title("Hello World")
# (resolution+pixels from left+pixels from top)
main_window.geometry("840x680+100+300")
# .mainLoop() passes control to tk this way window works

# we create component (specify window, add text)
label = tkinter.Label(main_window, text="Hello World")
# add using geometry manager specify row and column
label.grid(row=0, column=0)

# .frame(for what parent frame it belongs)
left_frame = tkinter.Frame(main_window)
left_frame.grid(row=1, column=1)

canvas = tkinter.Canvas(left_frame, relief='raised', borderwidth=1)
canvas.grid(row=1, column=0)
# canvas.pack(side='top', fill=tkinter.BOTH, expand=True)

right_frame = tkinter.Frame(main_window)
right_frame.grid(row=1, column=2, sticky='n')

# Button(for what frame belongs, text)
button_1 = tkinter.Button(right_frame, text="button1")
button_2 = tkinter.Button(right_frame, text="button2")
button_3 = tkinter.Button(right_frame, text="button3")
# anchor takes parameter n = north, s = south ...
button_1.grid(row=0, column=0)
button_2.grid(row=1, column=0)
button_3.grid(row=2, column=0)

# configure columns
main_window.columnconfigure(0, weight=1)
main_window.columnconfigure(1, weight=1)
main_window.grid_columnconfigure(2, weight=1)
left_frame.config(relief='sunken', borderwidth=1)
right_frame.config(relief='sunken', borderwidth=1)
left_frame.grid(sticky='ns')
right_frame.grid(sticky='new')

right_frame.columnconfigure(0, weight=1)
button_2.grid(sticky='ew')
main_window.mainloop()

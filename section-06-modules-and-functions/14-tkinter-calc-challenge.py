# Write a GUI program to create a simple calculator
# layout that looks like the screenshot.
#
# Try to be as Pythonic as possible - it's ok if you
# end up writing repeated Button and Grid statements,
# but consider using lists and a for loop.
#
# There is no need to store the buttons in variables.
#
# As an optional extra, refer to the documentation to
# work out how to use minsize() to prevent your window
# from being shrunk so that the widgets vanish from view.
#
# Hint: You may want to use the widgets .winfo_height() and
# winfo_width() methods, in which case you should know that
# they will not return the correct results unless the window
# has been forced to draw the widgets by calling its .update()
# method first.
#
# If you are using Windows you will probably find that the
# width is already constrained and can't be resized too small.
# The height will still need to be constrained, though.

import tkinter

# creates keys that represents calc structure
keys = [[('C', 1), ('CE', 2)],
        [('7', 1), ('8', 1), ('9', 1), ('+', 1)],
        [('4', 1), ('5', 1), ('6', 1), ('-', 1)],
        [('1', 1), ('2', 1), ('3', 1), ('*', 1)],
        [('0', 1), ('=', 1), ('/', 1), ('+', 1)]
        ]

# setup main window
main_window_padding = 8
main_window = tkinter.Tk()
main_window.title("My Calculator")
main_window.geometry('640x480-8-200')
main_window['padx'] = main_window_padding

# setup entry calc window
result = tkinter.Entry(main_window)
result.grid(row=0, column=0, sticky='nsew')

# setup key pad
key_pad = tkinter.Frame(main_window)
key_pad.grid(row=1, column=0, sticky='nsew')

row = 0
for key_row in keys:
    col = 0
    for key in key_row:
        tkinter.Button(
            key_pad, text=key[0]).grid(
            row=row, column=col, columnspan=key[1], sticky=tkinter.E + tkinter.W)
        col += key[1]
    row += 1

# setup min and max size of a window
main_window.update()
main_window.minsize(
    key_pad.winfo_width() + main_window_padding,
    result.winfo_height() + key_pad.winfo_height())
main_window.maxsize(
    key_pad.winfo_width() + main_window_padding,
    result.winfo_height() + key_pad.winfo_height())

main_window.mainloop()










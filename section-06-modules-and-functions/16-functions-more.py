# example of drawing parabola using functions

try:
    import tkinter
except ImportError:  # Python 2
    import Tkinter as tkinter


def parabola(x):
    y = x * x / 100
    return y


def draw_axes(canvas):
    canvas.update()
    x_origin = canvas.winfo_width() / 2
    y_origin = canvas.winfo_height() / 2
    canvas.configure(
        scrollregion=(-x_origin, -y_origin, x_origin, y_origin))
    canvas.create_line(-x_origin, 0, x_origin, 0, fill="black")
    canvas.create_line(0, y_origin, 0, -y_origin, fill="black")


def plot(canvas, x, y):
    canvas.create_line(x, y, x + 1, y + 1, fill="red")


main_window = tkinter.Tk()
main_window.title("Parabola")
main_window.geometry("1000x900")

canvas = tkinter.Canvas(main_window, width=500, height=900)
canvas.grid(row=0, column=0)

canvas_2 = tkinter.Canvas(main_window, width=500, height=900, background="blue")
canvas_2.grid(row=0, column=1)

draw_axes(canvas)
draw_axes(canvas_2)

for x in range(-100, 100):
    y = parabola(x)
    plot(canvas, x, -y)


main_window.mainloop()
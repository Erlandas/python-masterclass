# import time
#
# print(time.gmtime(0))
#
# # print(time.localtime())
# # time in ms from 1970
# # print(time.time())
#
# time_here = time.localtime()
# print(time_here)
# # Two ways to print data
# print("Year: {} - {}".format(time_here[0], time_here.tm_year))
# print("Moth: {} - {}".format(time_here[1], time_here.tm_mon))
# print("Day: {} - {}".format(time_here[2], time_here.tm_mday))

# BEFORE IMPROVEMENT
# import time
# from time import time as my_timer
# import random
#
# input("Press ENTER to start")
#
# wait_time = random.randint(1, 6)
# time.sleep(wait_time)
# start_time = my_timer()
# input("Press ENTER to stop")
#
# end_time = my_timer()
#
# print("Started at {}".format(time.strftime("%X", time.localtime(start_time))))
# print("Ended at {}".format(time.strftime("%X", time.localtime(end_time))))
# print("Your reaction time was {} seconds".format(end_time - start_time))

import time
# best function to measure elapsed time
# from time import perf_counter as my_timer

# monotonic means we cant go backwards in time
# useful if there is any adjustments for a computers clock
# from time import monotonic as my_timer

# process_time returns time of cpu how long it spends
# it used for profiling a code time spent
from time import process_time as my_timer
import random

input("Press ENTER to start")

wait_time = random.randint(1, 6)
time.sleep(wait_time)
start_time = my_timer()
input("Press ENTER to stop")

end_time = my_timer()

print("Started at {}".format(time.strftime("%X", time.localtime(start_time))))
print("Ended at {}".format(time.strftime("%X", time.localtime(end_time))))
print("Your reaction time was {} seconds".format(end_time - start_time))
